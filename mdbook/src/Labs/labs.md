## Submitting your Pseudocode Labs

**To complete your pseudocode tasks:**
1. ```Clone``` your personal ***JQR*** repository
2. Download this file and move to your ***JQR repo*** under `IDF/pseudocode/labs.md`
3. Create a merge request on *your* ***JQR repo*** for your work
4. ```git add IDF/pseudocode/labs.md ```
5. ```git commit -m "explain what you've done $(date)"```
6. ```git push```

---
  
### **Pseudocode Activity 1**
**Binary Pt. 1** 

*KSATs: K0130, K0714*

Use `Google` to generate two random numbers (or pick two numbers, your choice).  
**Number 1**:  
**Number 2**:  
**Number 1 in `binary`**:  
**Number 2 in `binary`**:  

**`AND`** value of **Number 1** `AND` **NUMBER 2**:  
**`OR`** value **Number 1** `OR` **NUMBER 2**:  
**`XOR`** value **Number 1** `XOR` **NUMBER 2**:   

---

### **Pseudocode Activity 2**
**Binary Pt. 2**

*KSATs: K0211*

Using your previous numbers:  
What is number one in binary:  
What is the value of number one - ``shifted right 2x``:  
What is the negative value (`1's complement`) of the result:  
What is the negative value (`2's complement`) of the result:

What is number two in binary:  
What is the value of number two - ``shifted left 2x``:  
What is the negative value (`1's complement`) of the result:  
What is the negative value (`2's complement`) of the result:

---

### **Pseudocode Activity 3**

*KSATs: A0004, S0171, S0172*

- Write pseudocode for a program that prints the numbers from 1 to 100. 
- For multiples of three print "Pop" instead of that number.
- For multiples of five print "Splash". 
- For numbers divisible by both three and five print "PopSplash"  


**Then you will create a flowchart using *mermaid***  
To install Mermaid for VSCode go [here](https://marketplace.visualstudio.com/items?itemName=vstirbu.vscode-mermaid-preview)  
More information on Mermaid is available [here](https://docs.gitlab.com/ee/user/markdown.html#diagrams-and-flowcharts-using-mermaid)  
and [here](https://mermaid-js.github.io/mermaid/#/flowchart)  

Basic flowchart symbols can be found [here](https://www.owlnet.rice.edu/~ceng303/manuals/fortran/FOR3_3.html)  

``You should start with these building blocks``

*Please note - some Mermaid options that work on your computer will not work when compiled on MD Books*

```mermaid
graph TB
   start[start] --> decision{decision} -.- no -.-> input[/input/];
   input -.-> procedure[procedure];
   
   procedure == yes ==> finish["end"]; 
   
```

Cf. `Guvf vf whfg Svmm Ohmm - Tbbtyr vg.`



## **Pseudocode Activity 4**

*KSATs: K0130, K0834 *

The following pseudocode has an error. 
Fix the error, then briefly explain the problem.
```
**DECLARE** double lowest, highest, average

**DISPLAY** "Enter the lowest score."
**INPUT** lowest

**DISPLAY** "Enter the highest score."
**INPUT** highest

**SET** average = low + high/2
**DISPLAY** "The average is ", average, "."
```

`Complete the following sentences-`  
*The problem with the original code was that...*  
*I fixed the code by...*  



## **Pseudocode Activity 5**

*KSATs: A0004, S0172*

Consider the "Puppy" example from the slideshow.   
Create pseudocode for a grades program that only accepts legitimate grades
***and does not crash*** on non-grades.  
Please use `markdown syntax` as appropriate.  [Check here](https://guides.github.com/features/mastering-markdown/)

Put your pseudocode here:  
`VVVVVV`
```
**RETURN** 'A'

```
`^^^^^^`

**`Create your flow chart here`**:  
```mermaid
 graph TD;
        A-->B;
        A-->C;
        B-->D;
        C-->D;
   
```

## **Pseudocode Activity 6**

*KSATs: K0130, K0834*

Explain briefly, what is the problem with the following code?  
Fix the code, then explain how you fixed the code and how that solved the problem.  

```
*_START_*:
**SUBROUTINE** getAverageofThree(a, b, c)
	total <-- a + b + c
  	avg = total/3
  	**RETURN** *avg*
**ENDSUBROUTINE**

**OUTPUT** "Enter 3 grades"
grade1 <-- *USERINPUT*
grade2 <-- *USERINPUT*
grade3 <-- *USERINPUT*
average <-- getAverageofThree(grade1, grade2, grade3)
**OUTPUT** "Total points were " + total
**OUTPUT** "The grade is " + avg

*_STOP_*
```


### **Pseudocode Activity 7**

*KSATs: A0004, S0172*

Consider the [flowchart](../assets/exceptionhandled_passwd.png) discussed to `avoid buffer overflow` and other attacks on password entry.  

Write the pseudocode for the correct program here:
```

```


## **Pseudocode Activity 8**

*KSATs: K0130, K0834*

Explain the problem with this code and fix it.
```

MODULE checkRange (Integer value, Integer lower, Integer upper)
    IF value < lower AND value > upper Then 
       Display "The value is outside the range."
    ELSE
       Display "The value is with the range."
    END IF 
END MODULE 

```


## **Pseudocode Activity 9**

*KSATs: A0004, S0171, S0172, K0130, K0834*

### From PseudoLab 5.1 
* Convert the following `While` loop to a `For` loop:
```
Declare Integer count = 0
While count < 50
   Display "The count is ", count
   Set count = count + 1
End While
```
* Convert the following For loop to a While loop:
```
Declare Integer count
For count = 1 To 50
   Display count
End For
```

## **Pseudocode Activity 10**

*KSATs: A0004, S0171, S0172, K0130, K0834*

Complete PseudoLab 6. 1 [here](./PseudoLab6.1.md)  
Add your answers to a new file `IDF/pseudocode/lab10.md`  


## **Pseudocode Activity 11**

*KSATs: A0004, S0171, S0172*

Select and complete a program from PseudoLab 6.2 [here](./PseudoLab6.2.md)  
Add your answers to a new file   ``YourLastName_firstInitial_PseucodeLab_6_2.md``

Place the link to the completed file here`->` 

## **Pseudocode Activity 12**

*KSATs: A0004, S0171, S0172, K0130, K0834*

Complete the Rock, Paper, Scissors Lab from PseudoLab 8.1 [here](./PseudoLab8.1.md)  
Add your answers to a new file   YourLastName_firstInitial_PseucodeLab_6_1.md  

Place the link to the completed file here`->` 



---
# Review Activities

## From PseudoLab 1##

* Find the error in the following pseudocode

```
DISPLAY "Enter the length of the room."
INPUT length 
DECLARE Integer length

```

---

* Find the error in the following pseudocode

```
DECLARE Integer value1, value2, value3, sum
SET sum = value1 + value2 + value3

DISPLAY "Enter the first value."
INPUT value1

DISPLAY "Enter the second value."
INPUT value2

DISPLAY "Enter the third value."
INPUT value3

DISPLAY "The sum of numbers is ", sum

```

---

* Find the error in the following pseudocode

```
DECLARE Real pi
SET 3.14159265 = pi
DISPLAY "The value of pi is ". pi
```

---

* Find the error in the following pseudocode.

```
CONSTANT Real GRAVITY = 9.81
DISPLAY "Rates of acceleration of an objet in free fall:"
DISPLAY "Earth: ", GRAVITY, " meters per second every second."
SET GRAVITY = 1.63
DISPLAY "Moon: ", GRAVITY, " meters per second every second."
```



---
# Challenge Activities

## From PseudoLab 4.2 
### Leap Year Detector

Design a program that asks the user to enter a year, and then displays a message indicating whether that year is a leap year or not. Use the following logic to develop your algorithm:

* If the year is evenly divisible by 100 and is also evenly divisible by 400, then it is a leap year. For example, 2000 is a leap year but 2010 is not.

* If the year is not evenly divisible by 100, but it is evenly divisible by 4, it is a leap year. For example, 2008 is a leap year but 2009 is not.

```

```


## From PseudoLab 7.1

Complete the *3* programs from PseudoLab 7.1 [here](./PseudoLab7.1.md)  
*But **NOT** the Rock, Paper, Scissors Lab*   
Add your answers to a new file  
```YourLastName_firstInitial_PseucodeLab_6_1.md```  

**Briefly describe the first program, here:**

*Place the link to the completed file here`->`* 

**Briefly describe the second program, here:**

*Place the link to the completed file here`->`* 

**Briefly describe the third program, here:**

*Place the link to the completed file here`->`* 

