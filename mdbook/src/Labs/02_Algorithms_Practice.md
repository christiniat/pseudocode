# Practice Exercises Big O Notation

## Objectives
### This contains exercises to practice Big O Notation concepts.

---

## Big O running time

Determine the Big-O-running time for the following code fragments.

1. 
![](../assets/2big_O_01.png)

---
2. 
![](../assets/2big_O_02.png)

---
3.
![](../assets/2big_O_03.png)

---
### Time Complexity: O(log/n): Logarthmic
Example operation: Finding an element in a sorted array.
Review the following examples:

1.
![](../assets/2big_O_04.png)

---

2.
![](../assets/2big_O_05.png)

3.
![](../assets/2big_O_06.png)

---
