# PSEUDOCODE

## An introduction to computers and programming
## Pseudocode 

**The pseudocode lessons are available [here](https://90cos.gitlab.io/training/modules/pseudocode)**

**The slides for pseudocode are available [here](https://90cos.gitlab.io/training/modules/pseudocode/slides/#/)**

**The gitlab exercises should be downloaded from [here](https://gitlab.com/90cos/training/modules/pseudocode)**  

The graded labs are found in mdbook/src/Labs/YourLastName_FirstInitial_PseudocodeLabs.md 


This unit introduces students to the foundational ideas of computer science.
In this unit, students will review fundamental the fundamental technologies and concepts underlying computer science.

The primary focus is familiarizing students with the logic and structures that computer science is used to solve - without relying on a particular syntax.


